//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
int CB=0;//ComboBox1的行數
TImage *data[50];//5*5最多50個圖片
TImage *picture[25],*last;//放入的圖片最多25張 ,
String picture2[25];//使放入的圖片不重複
int x=0,situation=0,situation2=0,bingo,time1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
  randomize();
}
//---------------------------------------------------------------------------



void __fastcall TForm1::Button1Click(TObject *Sender)
{  //開始,重玩
  int h,w,W,H;
  if(x>=2){ //最少加入兩張圖片
    for(int i=0;i<CB*CB;i++){    //刪除牌的資料
    delete data[i];
    delete data[i+25];
    }
  if(situation==0){           //開始和重玩時控制介面開關
  Timer1->Enabled=true;
  Panel1->Enabled=true;
  Panel2->Enabled=true;
  Panel3->Enabled=false;
  ComboBox1->Enabled=false;
  ComboBox2->Enabled=false;
  Button1->Enabled=false;
  situation=1;
  }else{
  bingo=0;
  ComboBox1->Enabled=true;
  ComboBox2->Enabled=true;
  Panel1->Enabled=false;
  Panel2->Enabled=false;
  Panel3->Enabled=true;
  situation=0;
  }

  time1=ComboBox2->ItemIndex*60;
  CB=ComboBox1->ItemIndex+2;   //最小2*2 所以加2對應
  h=Panel1->Height/CB;
  w=Panel1->Width/CB;
  H=Panel2->Height/CB;
  W=Panel2->Width/CB;

  for(int i=0;i<CB*CB;i++){
  data[i]=new TImage(Form1);
  data[i]->Stretch=true;
  data[i]->Parent=Panel1;
  data[i]->Picture=Image1->Picture;
  data[i]->Width=w;
  data[i]->Height=h;
  data[i]->Left=w*(i%CB);
  data[i]->Top=h*(i/CB);
  data[i]->OnClick=hideClick;
  }
  for(int i=0;i<CB*CB;i++){
  data[i+25]=new TImage(Form1);
  data[i+25]->Stretch=true;
  data[i+25]->Parent=Panel2;
  data[i+25]->Picture=Image1->Picture;
  data[i+25]->Width=w;
  data[i+25]->Height=h;
  data[i+25]->Left=W*(i%CB);
  data[i+25]->Top=H*(i/CB);
  data[i+25]->OnClick=hideClick;
  }
  for(int i=0;i<CB*CB;i++){
  data[i]->Tag=random(x);
  data[i+25]->Tag=data[i]->Tag;
  }
  int r,r2,m,m2,n=CB*CB;

    for(int j=0;j<CB*CB;j++){
    m=data[j]->Tag;
    r=random(n);
    data[j]->Tag=data[r]->Tag;
    data[r]->Tag=m;
    }

  for(int i=0;i<CB*CB;i++){
    m2=data[i+25]->Tag;
    r2=random(n)+25;
    data[i+25]->Tag=data[r2]->Tag;
    data[r2]->Tag=m2;
  }
  }else{
  ShowMessage("請加入兩張圖片以上");
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{

  if(OpenPictureDialog1->Execute()){  //避免加圖沒出現
    for(int i=0;i<x;i++){             //避免加入重複的圖片
      if(picture2[i]==OpenPictureDialog1->FileName){
      ShowMessage("輸入重複的圖片,請換張圖片");
      return;
      }
    }
  picture2[x]=OpenPictureDialog1->FileName;
  picture[x]=new TImage(Form1);
  picture[x]->Stretch=true;
  picture[x]->Parent=Panel3;
  picture[x]->Picture->LoadFromFile(OpenPictureDialog1->FileName);
  picture[x]->Tag=x;
  picture[x]->OnClick=hide2Click;
  picture[x]->Height=30;
  picture[x]->Width=30;

    if(x>13){        //13張換行
    picture[x]->Left=31*(x%14);
    picture[x]->Top=31;
    x++;
    }else{
    picture[x]->Left=31*x;
    x++;
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::hideClick(TObject *Sender)
{
  TImage *a=(TImage *)Sender;
  a->Picture=picture[a->Tag]->Picture;
  Button1->Enabled=false;
  a->Parent->Enabled=false;
  a->Enabled=false;
  if(situation2==0){
    last=a;
    situation2=1;
  }else{
    if(a->Tag==last->Tag){    //翻開的牌一樣,不蓋回
    bingo++;
    last->Parent->Enabled=true;
    a->Parent->Enabled=true;
    last->Enabled=false;
    a->Enabled=false;
    Button1->Enabled=true;
      if(bingo==CB*CB){         //全翻完
        ShowMessage("恭喜獲勝!!");
        Timer1->Enabled=false;
      }
    }
    if(a->Tag!=last->Tag){         //翻開的牌不一樣,蓋回
    Application->ProcessMessages(); //顯示問題
    Sleep(500);
    last->Picture=Image1->Picture;
    a->Picture=Image1->Picture;
    last->Parent->Enabled=true;
    a->Parent->Enabled=true;
    Button1->Enabled=true;
    a->Enabled=true;
    last->Enabled=true;
    }
    situation2=0;
  }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::hide2Click(TObject *Sender)
{
  TImage *z=(TImage *)Sender;        //刪除已加入的圖片
  int a=z->Tag;
  for(int i=a;i<x-1;i++){
    picture[i]->Picture=picture[i+1]->Picture;
    picture2[i]=picture2[i+1];
  }
  x--;
  Timer2->Enabled=true;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer2Timer(TObject *Sender)
{
   delete picture[x];
   picture2[x]=NULL;
   Timer2->Enabled=false;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Timer1Timer(TObject *Sender)
{  //倒數計時
  time1--;
  if(ComboBox2->ItemIndex==0){
    Label3->Caption="時間無限";
  }else{
    if(time1>=0){
    Label3->Caption=time1;
    }
    if(time1==0){
    ShowMessage("時間到了,結束遊戲");
    Timer1->Enabled=false;
    Panel1->Enabled=false;
    Panel2->Enabled=false;
    Button1->Enabled=true;
    }
  }
}
//---------------------------------------------------------------------------



