//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
int b;
int num=0;
ball *bb[1];   //球
int color[3]={clYellow,clLime,clRed};//磚塊的三種顏色
int score=0;    //分數
TShape *bri[15];      //15磚塊
bezel j;    //檔板

//---------------------------------------------------------------------------
  ball::ball(){
  ballH=8+random(5);              //長寬
  ballW=8+random(5);
  ballX=new TTimer(Form1);
  ballX->Interval=100;
  ballX->Enabled=false;
  ballX->OnTimer=Timer2Timer;


  s=new TShape(Form1);
  s->Parent=Form1;
  s->Left=530;
  s->Top=320;
  s->Brush->Color=(TColor)random(0xffffff);
  s->Shape=stCircle;
  s->Width=13;
  s->Height=s->Width;
  ballX->Enabled=true;
  }

  ball::~ball(){
  delete ballX;
  delete s;

  };

//---------------------------------------------------------------------------
    Brick::Brick(){
    BrickX=new TTimer(Form1);
    BrickX->Interval=100;
    BrickX->Enabled=false;


    for(int i=0;i<15;i++){
     BrickS=new TShape(Form1);
     BrickS->Parent=Form1;
     BrickS->Brush->Color=(TColor)color[random (3)];
     BrickS->Shape=stRectangle;
     BrickS->Width=90;
     BrickS->Height=50;
     BrickS->Left=25+105*(i%5);
     BrickS->Top=65*(i/5);
     bri[i]=BrickS;
     }
    }



    Brick::~Brick(){
    delete BrickS;
    };



//---------------------------------------------------------------------------
     bezel::bezel(){
     bezelX=new TTimer(Form1);
     bezelX->Interval=40;
     bezelX->Enabled=false;
     bezelX->OnTimer=Timer3Timer;

     bezelx=new TTimer(Form1);
     bezelx->Interval=40;
     bezelx->Enabled=false;
     bezelx->OnTimer=Timer5Timer;

     bezelS=new TShape(Form1);                    //藍色檔板
     bezelS->Parent=Form1;
     bezelS->Brush->Color=(TColor)(0x00FFFF00);
     bezelS->Shape=stRectangle;
     bezelS->Width=130;
     bezelS->Height=20;
     bezelS->Left=300;
     bezelS->Top=350;
     bezelX->Enabled=true;

     bezels=new TShape(Form1);          //紅色檔板
     bezels->Parent=Form1;
     bezels->Brush->Color=(TColor)(clRed);
     bezels->Shape=stRectangle;
     bezels->Width=130;
     bezels->Height=20;
     bezels->Left=130;
     bezels->Top=350;
     bezelx->Enabled=true;
     }

     bezel::~bezel(){
     delete bezelX;
     delete bezelS;
     }
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner): TForm(Owner)
{
     randomize();
     new Brick();
     new bezel();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  if(b<3){                            //最多三顆
  bb[b++]=new ball();
  }
  Button2->Enabled=false;

}
//---------------------------------------------------------------------------
void __fastcall ball::Timer2Timer(TObject *Sender)
{

   if(s->Left<=0||s->Left>550){              //邊界
     ballW=-ballW;
   }
   if(s->Top>350){                          //出界
      bb[num]->s->Visible=false;
      bb[num]->ballX->Enabled=false;
      num++;
      Form1->Button2->Enabled=true;
      }
       if(num==3){
       ShowMessage("你好爛");
       Form1->Button2->Enabled=false;
       }
   if(s->Top<=0){                       //邊界
     ballH=-ballH;
   }

   if(s->Left+13>=(j.bezelS->Left)&&(s->Left)<=(j.bezelS->Left+130)
     &&(s->Top+13)>=j.bezelS->Top){
     ballH=-ballH;
   }       //檔板和球碰撞
   if(j.bezelS->Top<=s->Top&&j.bezelS->Top+50>=s->Top+20&&
   (j.bezelS->Left+130)>=s->Left&&(j.bezelS->Left)<=(s->Left+20)){
   ballW=-ballW;
   }

   if(s->Left+13>=(j.bezels->Left)&&(s->Left)<=(j.bezels->Left+130)
     &&(s->Top+13)>=j.bezels->Top){
     ballH=-ballH;
   }
   if(j.bezels->Top<=s->Top&&j.bezels->Top+50>=s->Top+20&&
   (j.bezels->Left+130)>=s->Left&&(j.bezels->Left)<=(s->Left+20)){
   ballW=-ballW;
   }

  for(int i=0;i<15;i++){   //磚塊與球碰撞
    if(bri[i]->Visible==true){
      if(bri[i]->Top<=s->Top+20&&bri[i]->Top+50>=s->Top&&
      (bri[i]->Left+90)>=s->Left&&(bri[i]->Left)<=(s->Left+20)){

        if(bri[i]->Top<=s->Top+20&&bri[i]->Top+50>=s->Top&&
        (bri[i]->Left+90)>=s->Left+20&&(bri[i]->Left)<=(s->Left)){
        ballH=-ballH; //LEFT
           }
        if(bri[i]->Top<=s->Top&&bri[i]->Top+50>=s->Top+20&&
        (bri[i]->Left+90)>=s->Left&&(bri[i]->Left)<=(s->Left+20)){
        ballW=-ballW;
           }
     // clYellow,clLime,clRed
     //撞到變色
         if(bri[i]->Brush->Color==clYellow){
            bri[i]->Brush->Color=clLime;
            break;
         }
         if(bri[i]->Brush->Color==clLime){
            bri[i]->Brush->Color=clRed;
            break;
         }
         if(bri[i]->Brush->Color==clRed){
           bri[i]->Visible=false;
           score+=100;
           Form1->Label1->Caption=score;
             if(score==1500){
                bb[num]->s->Visible=false;
                bb[num]->ballX->Enabled=false;
               ShowMessage("你就棒");
               Form1->Button2->Enabled=false;

             }
         }
      }
      /*if((bri[i]->Left+50)>s->Left&&(bri[i]->Left)<(s->Left+20)){
      ballH=-ballH;
      bri[i]->Visible=false;
      break;
      }    */
    }
  }
   s->Left=s->Left-ballW;
   s->Top=s->Top-ballH;

  /*  if(br.BrickS->Top+30>=s->Top&&
         br.BrickS->Top<=s->Top){
     ballH=-ballH;
      }
   if(br.BrickS->Left<=s->Left+20&&
      br.BrickS->Left+50>=s->Left){
     ballW=-ballW;
      }   */


}
//---------------------------------------------------------------------------
void __fastcall bezel::Timer3Timer(TObject *Sender)
{                     //藍色檔板移動
  if(GetAsyncKeyState(VK_RIGHT)){bezelS->Left=bezelS->Left+10;}
  if(GetAsyncKeyState(VK_LEFT)){
   if(bezelS->Left<=bezels->Left+131){
     bezelS->Left=bezelS->Left+0;
     }else{
     bezelS->Left=bezelS->Left-10;
     }
  }
  //if(GetAsyncKeyState(VK_DOWN)){bezelS->Top=bezelS->Top+10;}
  //if(GetAsyncKeyState(VK_UP)){bezelS->Top=bezelS->Top-10;}
  
   if(bezelS->Left<=0){
     bezelS->Left=0;
   }
   if(bezelS->Left>=500){
     bezelS->Left=500;
   }
  /* if(bezelS->Top<=220){
     bezelS->Top=220;
   }
   if(bezelS->Top>=300){
     bezelS->Top=300;    
   }   */

}
//---------------------------------------------------------------------------
void __fastcall bezel::Timer5Timer(TObject *Sender)
{                       //紅色檔板移動
//左65右68
     if(bezels->Left<=0){
     bezels->Left=0;
   }
   if(bezels->Left>=500){
     bezels->Left=500;
   }
  if(GetAsyncKeyState(65)){bezels->Left=bezels->Left-10;}
  if(GetAsyncKeyState(68)){
   if(bezels->Left+131>=bezelS->Left){
     bezels->Left=bezels->Left+0;
   }else{
   bezels->Left=bezels->Left+10;
   }
  }
}
//---------------------------------------------------------------------------

