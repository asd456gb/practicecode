//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TButton *Button2;
        TLabel *Label1;
        TLabel *Label2;

        void __fastcall Button2Click(TObject *Sender);

private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
 class ball{
 friend class bezel;
 friend class Brick;
 public:
  ball();
  ~ball();

 private:
  TTimer *ballX;
  TShape *s;
  void __fastcall Timer2Timer(TObject *Sender);
  int ballH,ballW;
 };

 class bezel{
  friend class ball;
  public:
  bezel();
  ~bezel();

  private:
  TTimer *bezelX;
  TTimer *bezelx;
  TShape *bezelS;
  TShape *bezels;
  void __fastcall Timer3Timer(TObject *Sender);
  void __fastcall Timer5Timer(TObject *Sender);
 };

 class Brick{
 friend class ball;
  public:
   Brick();
   ~Brick();
  private:
   TTimer *BrickX;
   TShape *BrickS;
   // brick[30];
  };



//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
