// let localVideo = document.getElementById("local-video");
// let remoteVideo = document.getElementById("remote-video");

let callout = document.getElementById("callout");
let callin = document.getElementById("callin");
let acceptbtn = document.getElementById("acceptbtn");
let rejectbtn = document.getElementById("rejectbtn");
// callin.style.display = "none";
// rejectbtn.style.display = "none";
// remoteVideo.style.display = "none";
let remotePeer = {};
let localStream;

let anotherID;
let videoTrack;
let audioTrack;
let addname;
let newuser
let video = document.getElementById("myvideo");
let videos = document.getElementById("videos");

let myID = url.searchParams.get('username');
console.log("myID:" + myID);

function showUser() {
    document.getElementById("allname").innerHTML = "";
    for (let i = 0; i < addname.length; i++) {
        document.getElementById("allname").innerHTML += addname[i] + "<br>";
        newuser = addname[addname.length - 1]
        console.log(addname[addname.length - 1])
    }
}

function videoOpen() {
    videoTrack[0].enabled = true;

}
function videoClose() {
    videoTrack[0].enabled = false;

}
function audioOpen() {
    audioTrack[0].enabled = true;
}
function audioClose() {
    audioTrack[0].enabled = false;
}
function sendmyID() {
    ws.send(JSON.stringify({
        type: "idToServer",
        id: myID
    }));
}

// function reject() {
//     callin.style.display = "none";
//     acceptbtn.style.display = "none";
//     rejectbtn.style.display = "none";
//     wstwo.send(JSON.stringify({
//         type: "reject",
//         remoteID: anotherID
//     }));
// }
function call() {
    ws.send(JSON.stringify({
        type: "callout",
        localID: myID,
        remoteID: callout.value
    }));
    // if (myID == callout.value) {
    //     alert("不能打給自己");
    // } else {
    //     wstwo.send(JSON.stringify({
    //         type: "callout",
    //         localID: myID,
    //         remoteID: callout.value
    //     }));
    // }

}

// wstwo = new WebSocket(`wss://${window.location.hostname}:3000`);
navigator.mediaDevices.getUserMedia({ audio: true, video: true })
    .then((stream) => {
        localStream = stream;
        video.srcObject = stream;
        videoTrack = localStream.getVideoTracks();
        audioTrack = localStream.getAudioTracks();
    })
function accept(addname) {
    for (let i = 0; i < addname.length; i++) {
        if (addname[i] == myID) continue;
        remotePeer[addname[i]] = new RTCPeerConnection({
            iceServers: [{ url: "stun:stun.l.googlecom:19302" }]
        });
        localStream.getTracks().forEach((track) => {
            console.log("getTrack");
            remotePeer[addname[i]].addTrack(track, localStream);
        });
        let offerOption = {
            offerToReceiveAudio: 1,
            offerToReceiveVideo: 1
        };
        remotePeer[addname[i]].createOffer(offerOption)
            .then(desc => {
                console.log("create offer");
                remotePeer[addname[i]].setLocalDescription(desc, () => {
                    console.log("setLocalDescription");
                    ws.send(JSON.stringify({
                        type: "offer",
                        localID: myID,
                        roomID: ROOMID,
                        remoteID: addname[i],
                        SDP: remotePeer[addname[i]].localDescription
                    }));
                });
            });
        remotePeer[addname[i]].onicecandidate = (e) => {
            if (e.candidate) {
                ws.send(JSON.stringify({
                    type: "ice_candidate",
                    localID: myID,
                    remoteID: addname[i],
                    roomID:ROOMID,
                    candidate: e.candidate
                }));
            }
        }
        remotePeer[addname[i]].ontrack = (e) => {
            let remoteVideo = document.createElement("video");
            remoteVideo.setAttribute("autoplay", "true");
            remoteVideo.setAttribute("playsinline", "true");
            remoteVideo.id = connectionStr;
            remoteVideo.srcObject = e.streams[0];
            videos.appendChild(remoteVideo);
        }

    }
}

ws.onmessage = (e) => {
    let json = JSON.parse(e.data);
    switch (json.type) {
        case "idForServer":
            sendmyID();
            break;
        case "join":
            addname = json.idList;
            // document.getElementById("allname").innerHTML = "";
            // for (let i = 0; i < addname.length; i++) {
            //     document.getElementById("allname").innerHTML += addname[i] + "<br>";
            // }
            break;

        case "callin":
            anotherID = json.remoteID;
            console.log(anotherID);
            callin.style.display = "";
            acceptbtn.style.display = "";
            rejectbtn.style.display = "";
            document.getElementById("callin").innerHTML = anotherID + " call for " + myID;
            break;
        // case "reject":
        //     alert(callout.value + "拒接");
        //     break;
        // case "accept":
        //     remotePeer = new RTCPeerConnection({
        //         iceServers: [{ url: "stun:stun.l.googlecom:19302" }]
        //     });
        //     localStream.getTracks().forEach((track) => {
        //         console.log("getTrack");
        //         remotePeer.addTrack(track, localStream);
        //     });
        //     let offerOption = {
        //         offerToReceiveAudio: 1,
        //         offerToReceiveVideo: 1
        //     };
        //     remotePeer.createOffer(offerOption)
        //         .then(desc => {
        //             console.log("create offer");
        //             remotePeer.setLocalDescription(desc, () => {
        //                 console.log("setLocalDescription");
        //                 wstwo.send(JSON.stringify({
        //                     type: "offer",
        //                     localID: myID,
        //                     remoteID: json.newuser,
        //                     SDP: remotePeer.localDescription
        //                 }));
        //             });
        //         });
        //     remotePeer.onicecandidate = (e) => {
        //         if (e.candidate) {
        //             wstwo.send(JSON.stringify({
        //                 type: "ice_candidate",
        //                 localID: myID,
        //                 remoteID: json.newuser,
        //                 candidate: e.candidate
        //             }));
        //         }
        //         remotePeer.ontrack = (e) => {
        //             let remoteVideo=document.createElement("video");
        //             remoteVideo.setAttribute("autoplay","true");
        //             remoteVideo.setAttribute("playsinline","true");
        //             remoteVideo.id=connectionStr;
        //             remoteVideo.srcObject=e.streams[0];
        //             videos.appendChild(remoteVideo);

        //             // remoteVideo.srcObject = e.streams[0];
        //             // remoteVideo.style.display = '';
        //             // localVideo.className = "secondary-video";
        //             // remoteVideo.className = "primary-video";
        //         }
        //     }
        //     break;
        case "offer":
            remotePeer[json.user] = new RTCPeerConnection({
                iceServers: [{ url: "stun:stun.l.google.com:19302" }]
            });
            localStream.getTracks().forEach((track) => {
                console.log("getTrack");
                remotePeer[json.user].addTrack(track, localStream);
            });
            remotePeer[json.user].setRemoteDescription(json.SDP)
                .then(() => {
                    console.log("setRemoteDescription");
                    remotePeer[json.user].createAnswer()
                        .then((desc) => {
                            console.log("createAnswer");
                            remotePeer[json.user].setLocalDescription(desc, () => {
                                console.log("setLocalDescription");
                                ws.send(JSON.stringify({
                                    type: "answer",
                                    localID: myID,
                                    remoteID: json.user,
                                    roomID:ROOMID,
                                    SDP: remotePeer[json.user].localDescription
                                }));
                            });
                        });
                });
            remotePeer[json.user].onicecandidate = (e) => {
                console.log("oniceCandidate");
                ws.send(JSON.stringify({
                    type: "ice_candidate",
                    localID: myID,
                    remoteID: json.user,
                    roomID:ROOMID,
                    candidate: e.candidate

                }));
            }
            remotePeer[json.user].ontrack = (e) => {
                console.log("onTrack");
                remoteVideo.srcObject = e.streams[0];
                // remoteVideo.style.display = '';
                // localVideo.className = "secondary-video";
                // remoteVideo.className = "primary-video";

            }
            break;
        case "answer":
            remotePeer[json.user].setRemoteDescription(json.SDP);
            // remotePeer.onicecandidate = (e) => {
            //     console.log("oniceCandidate");
            //     wstwo.send(JSON.stringify({
            //         type: "ice_candidate",
            //         localID: myID,
            //         remoteID: json.newuser,
            //         candidate: e.candidate
            //     }));
            // }
            // remotePeer.ontrack = (e) => {
            //     console.log("onTrack");
            //     remoteVideo.srcObject = e.streams[0];
            //     remoteVideo.style.display = '';
            //     // localVideo.className = "secondary-video";
            //     // remoteVideo.className = "primary-video";
            // }
            break;
        case "ice_candidate":
            remotePeer[json.user].addIceCandidate(json.candidate);
            break;
        case "disconnect":
            addname = json.idList;
            document.getElementById("allname").innerHTML = "";
            for (let i = 0; i < addname.length; i++) {
                document.getElementById("allname").innerHTML += addname[i] + "<br>";
            }
            console.log(callout.value);
            console.log(json.disconnectid);
            if (callout.value == json.disconnectid || anotherID == json.disconnectid) {
                // localVideo.className = "primary-video";
                // remoteVideo.className = "secondary-video";
                // remoteVideo.style.display = "none";
            }
            break;
    }
}
