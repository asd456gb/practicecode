const https = require("https");
const ws = require("ws");
const fs = require("fs");
const express = require("express");
const { stripVTControlCharacters } = require("util");
const app = express();

const server = https.createServer(
    {
        cert: fs.readFileSync("./server.crt"),
        key: fs.readFileSync("./server.key")
    });
const serverone = https.createServer(
    {
        cert: fs.readFileSync("./server.crt"),
        key: fs.readFileSync("./server.key")
    });
const servertwo = https.createServer(
    {
        cert: fs.readFileSync("./server.crt"),
        ca: [fs.readFileSync('./cert.pem')],
        key: fs.readFileSync("./server.key")
    });
const serverthree = https.createServer(
    {
        cert: fs.readFileSync("./server.crt"),
        key: fs.readFileSync("./server.key")
    });

const wss = new ws.WebSocketServer(
    {
        server: server
    });
const wssone = new ws.WebSocketServer(
    {
        server: serverone
    });
const wsstwo = new ws.WebSocketServer(
    {
        server: servertwo
    });
const wssthree = new ws.WebSocketServer(
    {
        server: serverthree
    });


// PORT共同編輯3001 wss 聊天室3002 wssone 視訊3000 wsstwo 共享畫面3003 wssthree
//共同編輯
server.listen(3001);
app.use(express.static("public"));
server.on("request", app);
wss.on("connection", (ws, request) => {
    ws.on("message", (data) => {
        console.log(data.toString());
        //ws.send(data.toString());
        const cs = wss.clients;
        cs.forEach((client) => {
            if (client != ws)
                client.send(data.toString());
        })
        //ws.clients.forEach
    })
    ws.on('close', () => {
        const cs = wss.clients;
        cs.forEach((client) => {
            if (client != ws)
                client.send(JSON.stringify(
                    {
                        type: "br",
                        body: "1"
                    }));
        })
    })
})
//end共同編輯
//聊天室
serverone.listen(3002);
serverone.on("request", app);
let onlineCount = 0;

let roomlist = [];
// let room = {};
wssone.on("connection", (ws, request) => {
    ws.on("message", (data) => {
        let json = JSON.parse(data);
        // console.log("etet"+json.message);
        // console.log(json);
        switch (json.type) {
            case "join":
                let un = json.username;
                if (roomlist.length == 0) {
                    console.log("noroom");
                } else {
                    for (let i = 0; i < roomlist.length; i++) {
                        if (roomlist[i].roomName == json.roomname) {
                            console.log("HI");

                            roomlist[i].ws.push(ws);
                            roomlist[i].userName.push(un);
                            console.log(roomlist);

                        }
                        ws.send(JSON.stringify({
                            type: "joinc",
                            roomdata: roomlist[i].userName

                        }));
                    }
                }
        
        console.log(roomlist);
        break;

        case "create":
        let room = {};
        let rm = json.roomname;
        // roomName[rm]=[];   
        let flag = false;
        for (let i = 0; i < roomlist.length; i++) {
            if (roomlist[i].roomName == json.roomname) {
                flag = true;
                // console.log("fail");
                break;
            }
        }
        if (flag) {
            console.log("fail");
            // break;
        } else {
            room["roomName"] = rm;
            // room[i].roomName = rm;
            room["userName"] = [];
            room["ws"] = [];
            roomlist.push(room);

            console.log("create roomlist=");
            console.log(roomlist);
        }

        break;

            //聊天室    
            case "send":
        let msg = json.message;
        let user = json.username;
        for (let i = 0; i < roomlist.length; i++) {
            if (roomlist[i].roomName == json.roomname) {
                console.log(roomlist[i].ws.length);
                for (let j = 0; j < roomlist[i].ws.length; j++) {
                    roomlist[i].ws[j].send(JSON.stringify({
                        type: "sendmsg",
                        message: user + ":" + msg
                    }));
                }


            }
        }
        break;
            case "show":
        let temp = [];
        for (let i = 0; i < roomlist.length; i++) {
            temp.push(roomlist[i].roomName);
        }
        ws.send(JSON.stringify({
            type: "showUser",
            roomlist: temp
        }));

        break;
            ///視訊
            case "offer":
        for (let i = 0; i < roomlist.length; i++) {
            if (roomlist[i].roomName == json.roomID) {
                for (let j = 0; j < roomlist[i].userName.length; j++) {
                    if (roomlist[i].userName[j] == json.remoteID) {
                        roomlist[i].ws[j].send(JSON.stringify({
                            type: "offer",
                            SDP: json.SDP,
                            user: json.localID

                        }));
                    }
                }
            }
        }
        break;
            case "answer":
        for (let i = 0; i < roomlist.length; i++) {
            if (roomlist[i].roomName == json.roomID) {
                for (let j = 0; j < roomlist[i].userName.length; j++) {
                    if (roomlist[i].userName[j] == json.remoteID) {
                        roomlist[i].ws[j].send(JSON.stringify({
                            type: "answer",
                            SDP: json.SDP,
                            user: json.localID
                        }))
                    }
                }
            }
        }
        break;
            case "ice_candidate":
        for (let i = 0; i < roomlist.length; i++) {
            if (roomlist[i].roomName == json.roomID) {
                for (let j = 0; j < roomlist[i].userName.length; j++) {
                    if (roomlist[i].userName[j] == json.remoteID) {
                        roomlist[i].ws[j].send(JSON.stringify({
                            type: "ice_candidate",
                            user: json.localID,
                            candidate: json.candidate
                        }))
                    }
                }
            }
        }

            case "exit":
        for (let i = 0; i < roomlist.length; i++) {
            if (roomlist[i].roomName == json.roomname) {
                for (let j = 0; j < roomlist[i].ws.length; j++) {
                    if (roomlist[i].ws[j] == ws) {
                        roomlist[i].ws.splice(j, 1);
                        roomlist[i].userName.splice(j, 1);
                        console.log(roomlist);
                    }
                }
            }
        }

        break;
        // case "saveId":
        //     roomlist.roomId.push=json.roomId;
        //     wssone.clients.forEach((client) => {
        //     client.send(JSON.stringify(
        //         {
        //             type:"join",
        //             roomname:roomId
        //         }))
        //     });
        //case "create":

    }
        //console.log(data.toString());
        //ws.send(data.toString());
        // const csone = wssone.clients;
        // console.log("連線人數" + csone.size);
        // csone.forEach((client) => {
        //     client.send(data.toString());
        // })
        //ws.clients.forEach
    })
ws.on('close', () => {
    console.log("bye");

})
})
//END聊天室
// 視訊
servertwo.listen(3000);
servertwo.on('request', app);
let ids = [];
let wsList = [];
wsstwo.on('connection', (ws, req) => {
    wsList.push(ws);
    ws.send(JSON.stringify({
        type: "idForServer"
    }));
    ws.on("message", (data) => {
        let json = JSON.parse(data);
        console.log(json);
        switch (json.type) {
            case "idToServer":
                ids.push(json.id);
                wsstwo.clients.forEach((client) => {
                    client.send(JSON.stringify({
                        type: "join",
                        idList: ids
                    }));
                });
                break;

            case "callout":
                // for (let i = 0; i < ids.length; i++) {
                //     if (ids[i] == json.remoteID) {
                //         wsList[i].send(JSON.stringify({
                //             type: "callin",
                //             // remoteID: json.localID
                //             remoteID: roomlist
                //         }));
                //     }
                // }

                for (let i = 0; i < roomlist.length; i++) {
                    if (roomlist[i].roomName == json.remoteID) {
                        console.log(roomlist[i].roomName);
                        for (let j = 0; j < roomlist[i].ws.length; j++) {
                            roomlist[i].ws[j].send(JSON.stringify({
                                type: "callin",
                                remoteID: json.localID
                            }));

                        }
                    }
                }
                break;

            // case "accept":
            //     for (let i = 0; i < ids.length; i++) {
            //         if (ids[i] == json.localuser) {
            //             wsList[i].send(JSON.stringify({
            //                 type: "accept",
            //                 newuser: json.remoteID

            //             }));
            //         }
            //     }
            //     break;
            // case "reject":
            //     for (let i = 0; i < ids.length; i++) {
            //         if (ids[i] == json.remoteID) {
            //             wsList[i].send(JSON.stringify({
            //                 type: "reject"
            //             }));
            //         }
            //     }
            //     break;
            // case "offer":

            //         if (ids[i] == json.remoteID) {
            //             wsList[i].send(JSON.stringify({
            //                 type: "offer",
            //                 SDP: json.SDP,
            //                 newuser: json.remoteID
            //             }));
            //         }

            //     break;
            // case "answer":
            //     for (let i = 0; i < ids.length; i++) {
            //         if (ids[i] == json.remoteID) {
            //             // wsList[i].send(JSON.stringify({
            //             //     type: "answer",
            //             //     SDP: json.SDP
            //             // }));
            //             for (let j = 0; j < roomlist[0].ws.length; j++) {
            //                 roomlist[0].ws[j].send(JSON.stringify({
            //                     type: "answer",
            //                     SDP: json.SDP
            //                 }));
            //             }
            //         }
            //     }
            //     break;
            // case "ice_candidate":
            //     for (let i = 0; i < ids.length; i++) {
            //         if (ids[i] == json.remoteID) {
            //             console.log()
            //             wsList[i].send(JSON.stringify({
            //                 type: "ice_candidate",
            //                 candidate: json.candidate
            //             }));
            //         }
            //     }
        }
    });
    ws.on("close", (code, reason) => {
        for (let i = 0; i < ids.length; i++) {
            if (ws == wsList[i]) {
                let disconnectID = ids[i];
                wsList.splice(i, 1);
                ids.splice(i, 1);
                wsstwo.clients.forEach((client) => {
                    client.send(JSON.stringify({
                        type: "disconnect",
                        idList: ids,
                        disconnectid: disconnectID
                    }));
                });
            }
        }
    });
});
// END視訊
// 共享
serverthree.listen(3003);
serverthree.on("request", app);
wssthree.on("connection", (ws, request) => {
    ws.on("message", (data) => {
        console.log(data.toString());
        //ws.send(data.toString());
        const cstwo = wssthree.clients;
        cstwo.forEach((client) => {
            if (client != ws)
                client.send(data.toString());
        })
        //ws.clients.forEach
    })
    ws.on('close', () => {
        const cstwo = wssthree.clients;
        cstwo.forEach((client) => {
            if (client != ws)
                client.send(JSON.stringify(
                    {
                        type: "br",
                        body: "1"
                    }));
        })
    })
})

// END共享