const http=require('http');
const express=require('express');
const WebSocket=require('ws');
const WebSocketServer = WebSocket.Server;

const port=3000;

const app=express();
app.use(express.static('public'));

const server=http.createServer();
server.listen(port,()=>{
    console.log("listening at http://localhost:"+port);
})
server.on('request',app);

const wss=new WebSocketServer(
{
    server: server
});

function random4()
{
    let numArray=['1','2','3','4','5','6','7','8','9'];
    for(i=0;i<numArray.length;i++)
    {
        let randomNum=Math.floor(Math.random()*9);
        [numArray[i], numArray[randomNum]] = [numArray[randomNum], numArray[i]];
    }
    return numArray[0]+numArray[1]+numArray[2]+numArray[3];
}


let randomQuestion=random4();
console.log(randomQuestion);
let last="此數字目前沒人猜過";
wss.on('connection',(ws,req)=>
{
    let url=new URL("http://test.com"+req.url);
    wss.clients.forEach((client)=>{
        client.send(JSON.stringify(
            {
                message: "歡迎"+url.searchParams.get("username")+"加入遊戲!",
                result: "welcome"
            }));
    })
    ws.send(JSON.stringify({
        message: last,
        result: "last"
    }));
    ws.on('message',(data)=>
    {
        let myjson=JSON.parse(data);
        let message="";
        if(myjson.message.length!=randomQuestion.length)
            message="請輸入符合規則之數字(四位數的整數)";
        else
        {
            let a=0,b=0;
            for(i=0;i<randomQuestion.length;i++)
            {
                for(j=0;j<randomQuestion.length;j++)
                {
                    if(randomQuestion.substr(i,1)==myjson.message.substr(j,1))
                        (i==j)?a++:b++;
                }
            }
            last=message=myjson.message+"->"+a+"A"+b+"B";
            if(a==4)
            {
                ws.send(JSON.stringify({
                    message: "Congratulation!",
                    result: "win"
                }));
                randomQuestion=random4();
                console.log(randomQuestion);
                last="此數字目前沒人猜過";
                message+="\n恭喜"+myjson.username+"猜出答案，已經出新的數字，請大家踴躍猜吧!";
            }
        }
        wss.clients.forEach((client)=>{
            client.send(JSON.stringify({
                message: myjson.username+"說:"+myjson.message,
                result: "someoneSay"
            }));
            client.send(JSON.stringify({
                message:message,
                result: "systemMessae"
            }));
        });
    });
});