const http=require('http');
const express=require('express');
const WebSocket=require('ws');
const WebSocketServer = WebSocket.Server;

const port=3000;

const app=express();
app.use(express.static('public'));

const server=http.createServer();
server.listen(port,()=>{
    console.log("listening at http://localhost:"+port);
})
server.on('request',app);

const wss=new WebSocketServer(
{
    server: server
});

wss.on('connection',(ws,req)=>
{
    let url=new URL("http://test.com"+req.url);
    wss.clients.forEach((client)=>{
        client.send("歡迎"+url.searchParams.get("username")+"加入聊天室!");
    })
    ws.on('message',(data)=>
    {
        wss.clients.forEach((client)=>{
            let myjson=JSON.parse(data);
            client.send(myjson.username+"說:"+myjson.message);
        });
    });
});